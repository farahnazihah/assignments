package assignments.assignment3;

public class Jurnalis extends Manusia{
  	
    public Jurnalis(String name){
        // Buat constructor untuk Jurnalis.
        // Hint: Akses constructor superclass-nya
        super(name);
    }
    
    @Override
    public String toString() {
        return "JURNALIS " + this.getNama();
    }
}