package assignments.assignment3;

import java.io.*;

public class InputOutput {

    private BufferedReader br;
    private PrintWriter pw;
    private final String inputFile;
    private final String outputFile;
    private World world;

    public InputOutput(final String inputType, final String inputFile, final String outputType,
            final String outputFile) {
        // Buat constructor untuk InputOutput.
        this.inputFile = inputFile;
        this.outputFile = outputFile;
        setBufferedReader(inputType);
        setPrintWriter(outputType);
    }

    public void setBufferedReader(final String inputType) {
        // Membuat BufferedReader bergantung inputType (I/O text atau input terminal)
        if (inputType.equalsIgnoreCase("text")) {
            try {
                br = new BufferedReader(new FileReader(this.inputFile));
            } catch (final IOException e) {
                System.out.println(e);
            }
        } else {
            br = new BufferedReader(new InputStreamReader(System.in));
        }

    }

    public void setPrintWriter(final String outputType) {
        // Membuat PrintWriter bergantung inputType (I/O text atau output terminal)
        if (outputType.equalsIgnoreCase("text")) {
            try {
                pw = new PrintWriter(new File(this.outputFile));
            } catch (final FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            pw = new PrintWriter(System.out, true);
        }
    }

    public void run() throws IOException {
        //  Program utama untuk InputOutput

        
        world = new World();
        String line;
        String[] line_array;
        try {
            while ((line = br.readLine()) != null) {
                line_array = line.split(" ");
    
                if (line_array[0].equalsIgnoreCase("ADD")) { // 1
                    world.createObject(line_array[1], line_array[2]);
    
                } else if (line_array[0].equalsIgnoreCase("INTERAKSI")) { // 2
                    Carrier obj1 = world.getCarrier(line_array[1]);
                    Carrier obj2 = world.getCarrier(line_array[2]);
                    obj2.interaksi(obj1);
    
                } else if (line_array[0].equalsIgnoreCase("POSITIFKAN")) { // 3
                    Carrier obj = world.getCarrier(line_array[1]);
                    obj.ubahStatus("positif");
    
                } else if (line_array[0].equalsIgnoreCase("SEMBUHKAN")) { // 4
                    PetugasMedis obj1 = (PetugasMedis) world.getCarrier(line_array[1]);
                    Manusia obj2 = (Manusia) world.getCarrier(line_array[2]);
                    obj1.obati(obj2);
    
                } else if (line_array[0].equalsIgnoreCase("BERSIHKAN")) { // 5
                    CleaningService obj1 = (CleaningService) world.getCarrier(line_array[1]);
                    Benda obj2 = (Benda) world.getCarrier(line_array[2]);
                    obj1.bersihkan(obj2);
    
                } else if (line_array[0].equalsIgnoreCase("RANTAI")) { // 6
                    Carrier obj = world.getCarrier(line_array[1]);
                    if (obj.getStatusCovid().equalsIgnoreCase("positif")) {
                        String rantaiString = String.format("Rantai penyebaran %s: ", obj.toString());
                        for (int i = 0; i < obj.getRantaiPenular().size(); i++) {
                            rantaiString += obj.getRantaiPenular().get(i).toString() + " -> ";
                        }
                        rantaiString += obj.toString();
                        pw.println(rantaiString);
                    } else {
                        String result = obj.toString() + " berstatus negatif";
                        try {
                            throw new BelumTertularException(result);
                        } catch (BelumTertularException e) {
                            pw.println(e);
                        }
                    }
    
                } else if (line_array[0].equalsIgnoreCase("TOTAL_KASUS_DARI_OBJEK")) { // 7
                    Carrier obj = world.getCarrier(line_array[1]);
                    String result = String.format("%s telah menyebarkan virus COVID ke %d objek", obj.toString(), obj.getTotalKasusDisebabkan());
                    pw.println(result);
    
                } else if (line_array[0].equalsIgnoreCase("AKTIF_KASUS_DARI_OBJEK")) { // 8
                    Carrier obj = world.getCarrier(line_array[1]);
                    String result = String.format("%s telah menyebarkan virus COVID dan masih terindikasi positif sebanyak %d objek", obj.toString(), obj.getAktifKasusDisebabkan());
                    pw.println(result);
    
                } else if (line_array[0].equalsIgnoreCase("TOTAL_SEMBUH_MANUSIA")) { // 9
                    String result = String.format("Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: %d kasus", Manusia.getJumlahSembuh());
                    pw.println(result);
    
                } else if (line_array[0].equalsIgnoreCase("TOTAL_SEMBUH_PETUGAS_MEDIS")) { // 10
                    PetugasMedis obj = (PetugasMedis) world.getCarrier(line_array[1]);
                    String result = String.format("%s menyembuhkan %d manusia", obj.toString(), obj.getJumlahDisembuhkan());
                    pw.println(result);
    
                } else if (line_array[0].equalsIgnoreCase("TOTAL_BERSIH_CLEANING_SERVICE")) { // 11
                    CleaningService obj = (CleaningService) world.getCarrier(line_array[1]);
                    String result = String.format("%s membersihkan %d benda", obj.toString(), obj.getJumlahDibersihkan());
                    pw.println(result);
                    
    
                } else if (line_array[0].equalsIgnoreCase("EXIT")) { // 12
                    break;
                }
    
            }
        } catch (Exception FileNotFoundException) {
            pw.println("File tidak ditemukan.");
        }
        pw.close();

    }
    
}