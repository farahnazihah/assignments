package assignments.assignment3;

public class PekerjaJasa extends Manusia{
  	
    public PekerjaJasa(String nama){
    	// Buat constructor untuk Jurnalis.
        // Hint: Akses constructor superclass-nya
        super(nama);
    }
    
    @Override
    public String toString() {
        return "PEKERJA JASA " + this.getNama();
    }
}