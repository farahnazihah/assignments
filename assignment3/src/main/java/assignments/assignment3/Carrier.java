package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public abstract class Carrier{

    private String nama;
    private String tipe;
    private Status statusCovid;
    private int aktifKasusDisebabkan;
    private int totalKasusDisebabkan;
    private List<Carrier> rantaiPenular;

    public Carrier(String nama, String tipe) {
        // Buat constructor untuk Carrier.
        this.nama = nama;
        this.tipe = tipe;
        this.statusCovid = new Negatif();
        this.rantaiPenular = new ArrayList<>();
        this.aktifKasusDisebabkan = 0;
        this.totalKasusDisebabkan = 0;
    }

    public String getNama() {
        // Kembalikan nilai dari atribut nama
        return this.nama;
    }

    public String getTipe() {
        // Kembalikan nilai dari atribut tipe
        return this.tipe;
    }

    public String getStatusCovid() {
        // Kembalikan nilai dari atribut statusCovid
        return this.statusCovid.getStatus();
    }

    public int getAktifKasusDisebabkan() {
        // Kembalikan nilai dari atribut aktifKasusDisebabkan
        return this.aktifKasusDisebabkan;
    }

    public int getTotalKasusDisebabkan() {
        // Kembalikan nilai dari atribut totalKasusDisebabkan
        return this.totalKasusDisebabkan;
    }

    public List<Carrier> getRantaiPenular() {
        // Kembalikan nilai dari atribut rantaiPenular
        return this.rantaiPenular;
    }

    public void ubahStatus(String status) {
        // Implementasikan fungsi ini untuk mengubah atribut dari statusCovid
        if (status.equalsIgnoreCase("Negatif")) {
            this.statusCovid = new Negatif();
            for (Carrier carrier : this.rantaiPenular) {
                if (!carrier.getNama().equalsIgnoreCase(this.getNama())) {
                    carrier.setAktifKasus(carrier.getAktifKasusDisebabkan() - 1);
                }
            }

        } else if (status.equalsIgnoreCase("Positif")) {
            this.statusCovid = new Positif();
        }
    }

    public void interaksi(Carrier lain) {
        // Objek ini berinteraksi dengan objek lain
        if (this.getStatusCovid().equalsIgnoreCase("negatif") || lain.getStatusCovid().equalsIgnoreCase("negatif")) {
            if (this.getStatusCovid().equalsIgnoreCase("positif")) {
                
                this.statusCovid.tularkan(this, lain);

                if (lain.getStatusCovid().equalsIgnoreCase("positif")) {
                    lain.rantaiPenular.clear();
                    lain.rantaiPenular.addAll(this.getRantaiPenular());
                    lain.rantaiPenular.add(this);

                    for (Carrier carrier : lain.getRantaiPenular()) {
                        carrier.setTotalKasus(carrier.getTotalKasusDisebabkan() + 1);
                        carrier.setAktifKasus(carrier.getAktifKasusDisebabkan() + 1);
                    }
                }

            } else if (lain.getStatusCovid().equalsIgnoreCase("positif")) {
                
                lain.statusCovid.tularkan(lain, this);
                
                if (this.getStatusCovid().equalsIgnoreCase("positif")) {
                    this.rantaiPenular.clear();
                    this.rantaiPenular.addAll(lain.getRantaiPenular());
                    this.rantaiPenular.add(lain);
                    
                    for (Carrier carrier : this.getRantaiPenular()) {
                        if(!carrier.getNama().equalsIgnoreCase(this.getNama())) {
                            carrier.setTotalKasus(carrier.getTotalKasusDisebabkan() + 1);
                            carrier.setAktifKasus(carrier.getAktifKasusDisebabkan() + 1);
                        }
                    }
                }
            }
        }
    }

    public abstract String toString();

    public void setAktifKasus(int aktifKasus) {
        this.aktifKasusDisebabkan = aktifKasus;
    }

    public void setTotalKasus(int totalKasus) {
        this.totalKasusDisebabkan = totalKasus;
    }
}
