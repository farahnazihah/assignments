package assignments.assignment3;

public class CleaningService extends Manusia{
  		
    private int jumlahDibersihkan;

    public CleaningService(String nama) {
        // Buat constructor untuk CleaningService.
        // Hint: Akses constructor superclass-nya
        super(nama);
        this.jumlahDibersihkan = 0;
    }

    public void bersihkan(Benda benda) {
        // Implementasikan apabila objek CleaningService ini membersihkan benda
        // Hint: Update nilai atribut jumlahDibersihkan
        benda.ubahStatus("negatif");
        benda.setPersentaseMenular(0);
        this.jumlahDibersihkan++;
    }

    public int getJumlahDibersihkan() {
        // Kembalikan nilai dari atribut jumlahDibersihkan
        return this.jumlahDibersihkan;
    }
    
    @Override
    public String toString() {
        return "CLEANING SERVICE " + this.getNama();
    }
}