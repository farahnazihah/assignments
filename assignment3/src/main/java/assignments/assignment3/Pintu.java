package assignments.assignment3;

public class Pintu extends Benda{
    // Implementasikan abstract method yang terdapat pada class Benda  
    
    public Pintu(String name){
        // Implementasikan apabila objek CleaningService ini membersihkan benda
        // Hint: Update nilai atribut jumlahDibersihkan
        super(name);
    }

    public void tambahPersentase() {
        this.persentaseMenular += 30;
    }
    
    @Override
    public String toString() {
        return "PINTU " + this.getNama();
    }
}