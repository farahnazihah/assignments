package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class World{

    public List<Carrier> listCarrier;

    public World(){
        // Buat constructor untuk class World
        this.listCarrier = new ArrayList<>();
    }

    public Carrier createObject(final String tipe, final String nama) {
        // Implementasikan apabila ingin membuat object sesuai dengan parameter yang diberikan
        Carrier obj;

        if (tipe.equalsIgnoreCase("Angkutan_Umum")) {
            obj = new AngkutanUmum(nama);
        } else if (tipe.equalsIgnoreCase("Tombol_Lift")) {
            obj = new TombolLift(nama);
        } else if (tipe.equalsIgnoreCase("Pintu")) {
            obj = new Pintu(nama);
        } else if (tipe.equalsIgnoreCase("Pegangan_Tangga")) {
            obj = new PeganganTangga(nama);
        } else if (tipe.equalsIgnoreCase("Cleaning_Service")) {
            obj = new CleaningService(nama);
        } else if (tipe.equalsIgnoreCase("Ojol")) {
            obj = new Ojol(nama);
        } else if (tipe.equalsIgnoreCase("Jurnalis")) {
            obj = new Jurnalis(nama);
        } else if (tipe.equalsIgnoreCase("Pekerja_Jasa")) {
            obj = new PekerjaJasa(nama);
        } else {
            obj = new PetugasMedis(nama);
        }

        this.listCarrier.add(obj);

        return obj;
    }

    public Carrier getCarrier(final String nama) {
        // Implementasikan apabila ingin mengambil object carrier dengan nama sesuai dengan parameter
        if (listCarrier != null) {
            for (final Carrier carrier : listCarrier) {
                if (carrier.getNama().equalsIgnoreCase(nama))
                    return carrier;
            }
        }
        return null;
    }
}