package assignments.assignment3;

public class PeganganTangga extends Benda{
    // Implementasikan abstract method yang terdapat pada class Benda
  	
    public PeganganTangga(String name){
        // Buat constructor untuk Jurnalis.
        // Hint: Akses constructor superclass-nya
        super(name);
        this.persentaseMenular = 0;
    }

    public void tambahPersentase() {
        this.persentaseMenular += 20;
    }
    
    @Override
    public String toString() {
        return "PEGANGAN TANGGA " + this.getNama();
    }
}