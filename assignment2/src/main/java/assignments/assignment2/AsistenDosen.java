package assignments.assignment2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AsistenDosen {
    private List<Mahasiswa> mahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    public AsistenDosen(String kode, String nama) {
        this.kode = kode;
        this.nama = nama;
    }

    public String getKode() {
        // kembalikan kode AsistenDosen.
        return this.kode;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        // tambahkan mahasiswa ke dalam daftar mahasiswa dengan mempertahankan urutan.
        // Hint: kamu boleh menggunakan Collections.sort atau melakukan sorting manual.
        // Note: manfaatkan method compareTo pada Mahasiswa.
        this.mahasiswa.add(mahasiswa);
        Collections.sort(this.mahasiswa);
    }

    public Mahasiswa getMahasiswa(String npm) {
        // kembalikan objek Mahasiswa dengan NPM tertentu dari daftar mahasiswa.
        // Note: jika tidak ada, kembalikan null atau lempar sebuah Exception.
        for (Mahasiswa i : this.mahasiswa) {
            if (i.getNpm().equals(npm)) {
                return i;
            }
        }
        return null;
    }

    public String rekap() {
        String rekap = "";
        rekap += "--- Rekap ---\n\n";
        rekap += this.toString() + "\n" + "~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n";
        for (Mahasiswa i : mahasiswa) {
            rekap += i.toString() + "\n";
            rekap += i.rekap() + "\n";
        }
        return rekap;
    }

    public String toString() {
        return this.kode + " - " + this.nama;
    }
}
