package assignments.assignment1;

import java.lang.Math;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class HammingCode {
    static final int ENCODE_NUM = 1;
    static final int DECODE_NUM = 2;
    static final int EXIT_NUM = 3;

    /**
     * encode melakukan proses encode dati data ke code.
     * @param data string data yang belum diproses
     * @return String
     */
    public static String encode(final String data) {
        int redundants = 2;

        while (Math.pow(2,redundants) < (data.length() + redundants + 1)) {
            redundants++;
        }

        int lengthOfCode = redundants + data.length();

        // make array for code
        String[] arrayCode = new String[lengthOfCode];
        int n = 0;

        for (int i = 0; i < lengthOfCode; i++) {
            if (isPowerOfTwo(i + 1)) {
                arrayCode[i] = "0";
            } else {
                arrayCode [i] = "" + data.charAt(n);
                n += 1;
            }
        }

        System.out.println(Arrays.toString(arrayCode));
        ArrayList<Integer> arrayError = parityCounter(arrayCode, redundants);

        for (int element:arrayError) {
            if (arrayCode[element - 1].equals("1")) {
                arrayCode[element - 1] = "0";
            } else if (arrayCode[element - 1].equals("0")) {
                arrayCode[element - 1] = "1";
            }
        }

        String code = "";
        for (String x:arrayCode) {
            code += x;
        }
        
        return code;
    }

    /**
     * fungsi decode memeproses code untuk mengambil data.
     * @param code kode yang akan diterjemahkan
     * @return String
     */
    public static String decode(final String code) {
        
        ArrayList<String> arrayCode = new ArrayList<String>();
        for (int i = 0; i < code.length(); i++) {
            String temp = "" + code.charAt(i);
            arrayCode.add(i, temp);
        }
        
        int redundants = 2;

        while (Math.pow(2,redundants) < (code.length() + redundants + 1)) {
            redundants++;
        }

        String[] temp = new String[arrayCode.size()];
        arrayCode.toArray(temp);
        ArrayList<Integer> arrayError = parityCounter(temp, redundants);

        int index = 0;
        for (int e:arrayError) {
            index += e;
        }
        
        if (arrayError.size() != 0) {
            if (arrayCode.get(index - 1).equals("0")) {
                arrayCode.set(index - 1, "1");
            } else if (arrayCode.get(index - 1).equals("1")) {
                arrayCode.set(index - 1, "0");
            }
    
        }
        
        for (int i = arrayCode.size() - 1; i >= 0; i--) {
            if (isPowerOfTwo(i + 1)) {
                arrayCode.remove(i);
            }
        }
    
        String encoded = "";
        for (String x:arrayCode) {
            encoded += x;
        }
    
        return encoded;
    }

    /**
     * fungsi untuk mengecek suatu bilangan apakah hasil dari dua pangkat.
     * @param x angka yang akan dicek
     * @return boolean
     */
    public static boolean isPowerOfTwo(double x) {
        if (x == 1) {
            return true;
        } else if (x < 1) {
            return false;
        } else {
            return isPowerOfTwo(x / 2);
        }
    }

    /**
     * fungsi untuk mencari bit yang belum sesuai.
     * @param data array yang akan dicari bit errornya
     * @return ArrayList
     */
    public static ArrayList<Integer> parityCounter(String[] data, int r) {
        ArrayList<Integer> arrayError = new ArrayList<Integer>();
        for (int i = 0; i < r; i++) {
            int n = (int) Math.pow(2,i);
            int counter = 0;
            int parbit = 0;
            for (int j = n - 1; j < data.length; j++) {
                parbit++;
                if (data[j].equals("1")) {
                    counter++;
                }
                if (parbit == n) {
                    parbit = 0;
                    j += n;
                }
            } 
            if (counter % 2 == 1) {
                arrayError.add(n);
            }
             
        }
        return arrayError;
        
    }
    
    /**
     * Main program for Hamming Code.
     * @param args unused
     */

    public static void main(String[] args) {
        System.out.println("Selamat datang di program Hamming Code!");
        System.out.println("=======================================");
        Scanner in = new Scanner(System.in);
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println();
            System.out.println("Pilih operasi:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            System.out.println("3. Exit");
            System.out.println("Masukkan nomor operasi yang diinginkan: ");
            int operation = in.nextInt();
            if (operation == ENCODE_NUM) {
                System.out.println("Masukkan data: ");
                String data = in.next();
                String code = encode(data);
                System.out.println("Code dari data tersebut adalah: " + code);
            } else if (operation == DECODE_NUM) {
                System.out.println("Masukkan code: ");
                String code = in.next();
                String data = decode(code);
                System.out.println("Data dari code tersebut adalah: " + data);
            } else if (operation == EXIT_NUM) {
                System.out.println("Sampai jumpa!");
                hasChosenExit = true;
            }
        }
        in.close();
    }
}
